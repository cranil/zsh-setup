
function if_git() {
    if [ -n "$USE_NERD_FONTS" ];
    then
        nf_gitlab_sym='\uf296'
        nf_github='\uf09b'
    else
        nf_gitlab_sym=''
        nf_github=''
    fi
    function git_dirty() {
        porcelain=$(git status --porcelain 2>/dev/null)
        n_added=$(expr `grep "^M" <<< $porcelain | wc -l`)
        n_modified=$(expr `grep "^ M" <<< $porcelain | wc -l`)
        n_untracked=$(expr `grep "^??" <<< $porcelain | wc -l`)
        if [[ "$(($n_added + $n_untracked + $n_modified))" == '0' ]]; then
            print -P "%F{green}✓%f"
        else
            print "%F{red}✗%f"
        fi
    }
    
    if git rev-parse --git-dir > /dev/null 2>&1; then
        branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
        url=$(git remote get-url origin 2>/dev/null)
        if [ -z "$url" ]; then
            host="local"
        elif [[ $url == *"@"* ]]; # check if using ssh for git
        then
            # host=${url%%:*} # gets you `git@hostname'
            # host=${url#*@}   # gets you `hostname' (everything after @)
            # combining them:
            host=${${url#*@}%%:*}
            repo=${url#*:}
            combined="${host}${repo}"
        else
            combined=${${url#*:}:2} # remove `https://'
            host=${combined%%/:}    # get hostname
            repo=${combined#*/}     # get repo
        fi
        if [ -n NERD_FONTS_DIR ]; then # check if nerd fonts is installed
            
            if [[ $host == *"github"* ]];
            then
                host="${nf_github} "
            elif [[ $host == *"gitlab"* ]];
            then
                host="${nf_gitlab} "
            else
                host=$host/
            fi
        fi
        print "%F{yellow}( $host${repo}:${branch} )%f $(git_dirty) "
    else
        if [ -n "$PRINT_DIR" ]; then
            print ''
        else
            print ''
        fi
        # print ${PRINT_DIR:}
    fi
}
