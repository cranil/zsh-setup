autoload -U compinit
compinit
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'

setopt PROMPT_SUBST

if [ -n "$USE_NERD_FONTS" ]; then
    server_ic='\uf473'
    client_ic='\uf109'
else
    server_ic=''
    client_ic=''
fi
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
    if (( ${+HOSTNAME} )); then
    else
        HOSTNAME="$(hostname)"
    fi
    HSTNAME="${server_ic} %Ussh://$HOSTNAME/%u"
    # many other tests omitted
else
    case $(ps -o comm= -p $PPID) in
        sshd|*/sshd) "${server_ic} %Ussh://$HOSTNAME/%u";;
    esac
    HSTNAME="${client_ic} $(hostname)"
fi

source "${HOME}/.zgen/zgenom.zsh"
if ! zgenom saved; then
    # specify plugins here
    zgenom oh-my-zsh
    zgenom oh-my-zsh plugins/git
    zgenom oh-my-zsh plugins/sudo
    zgenom oh-my-zsh plugins/command-not-found
    zgenom oh-my-zsh plugins/pip
    zgenom oh-my-zsh plugins/virtualenv
    zgenom oh-my-zsh plugins/rust
    # generate the init script from plugins above
    zgenom load zsh-users/zsh-autosuggestions
    zgenom save
fi

function prompt {
    function join_by {
        local d=${1-} f=${2-}
        if shift 2; then
            printf %s "$f" "${@/#/$d}"
        fi
    } # https://stackoverflow.com/a/17841619
    
    function resultf {
        if [[ $(print -P '%?') == "0" ]]; then
            print "😺"
        else
            print "😾"
        fi
    }
    
    function lpromptf {
        if [[ $? == "0" ]]; then
            print -P ""
        else
            print -P "\n"
        fi
    }
    
    function pyenv {
        python_env=$(basename "${VIRTUAL_ENV}")
        [[ -z "$python_env" ]] || python_env=" %f%F{green}🐍 $python_env %f%F{red}|"
        print -P $python_env
    }
    
    function shell_props {
        res=$(resultf)
        py=$(pyenv)
        delim=" %F{red}|%f "
        print -P $(join_by $delim $res $HSTNAME $py)
    }
    
    function dir_props {
        delim=" %F{green}|%f "
        git_info=$(print -P '$(if_git)')
        [[ -n "$USE_NERD_FONTS" ]] && dir_ic="" || dir_ic="🗂 "
        dir=$(print -P '%F{green}${dir_ic}%~%f')
        l=($git_info $dir)
        print -P $(join_by $delim $l)
    }
    
    echo "%F{blue}%B╭%b─%f$(lpromptf)%F{blue}[%f $(shell_props) %F{blue}|%f $(dir_props) %F{blue}]%f\n%F{blue}%B╰─%b[%f%F{green}%*%f%F{blue}]%f%F{red} ➜ %f "
}

export PROMPT='$(prompt)'

bindkey -r "^[^["
bindkey "^[^[[D" backward-word
bindkey "^[^[[C" forward-word

